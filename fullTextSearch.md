# Full-Text Search

### **ABSTRACTION**

Full-Text search refers to techniques for searching a single computer-stored document or a collection in a full-text database. Full-text search is distinguished from searches based on metadata or on parts of the original texts represented in databases (such as titles, abstracts,  or selected sections). It is an important branch of modern information retrieval technology. It is not only an important tool for dealing with unstructured data but also one of the mainstream technology of search engines. The main problem the existing search engines have to deal with how to avoid irrelevant information and to retrieve the relevant ones. The current work presents a new approach for retrieving relevant information on the web by adopting the first search algorithm.
 <br>
 <br>

### **1. DEFINATION**

In a full-text search, a search engine examines all of the words in every stored document as it tries to match search criteria. It is a technique that became common in an online database in the 1990s. Many websites and application programs provide full-text capabilities, while other indexes only a portion of the web pages examined by their indexing system.
<br>

Search engines are designed to help users quickly find useful information on the web with a number of search engines on the web and each different indexing method and different method and different coverage. Previous studies show that the performance of search engines depends on the performance measures used and the application domains. The performance of search engines can be evaluated using various measures such as precision, coverage response time, recall, and interface.
<br>
<br>

### **2. Full-Text Search Engine**

The full-Text Search Engine model is usually divided into two functional modules: Indexing Process and Query Process. The main task of the indexing process is the index for the specified processing resource, such as web pages and other types of documents on the internet the content of the resource.
<br>
<br>

### **3. INDEXING** 

When dealing with a small number of documents, it is possible for the full-text search engine to directly scan the contents of the documents with each query, a strategy called **Serial Scanning**. <br>The quantity of search queries to perform is substantial. The problem of full-text search is often divided into two tasks: Indexing and Searching. The indexing stage will scan the text of all the documents and build a list of search terms called an index, but more correctly named a **Concordance**. In the search stage, when performing a specific query, only the index refers rather than the text of the original documents.
<br>
<br>

### **4. THE RECALL VS PRECISION TRADE-OFF**

Recall measures the number of relevant results returned by a search, while precision is the measure of the quality of the results returned. The recall is the ratio of relevant results returned to all relevant results. Precision is the number of relevant results returned to the total number of results returned.<br>Due to the ambiguities of natural language, full-text-search systems typically include options like stop word increase precision and stemming increase recall controlled vocabulary searching also helps alleviate low precision issues by tagging documents in such a way that ambiguities are eliminated. The trade-off between precision and recall is simple: an increase in precision can lower overall recall, while an increase in recall lowers precision.
<br>
<br>

### **5. FALSE-POSITIVE PROBLEM**

Full-text searching is likely to retrieve many documents that are not relevant to the intended search question documents are called **False Positive**. The retrieval of irrelevant documents is often caused by the inherent ambiguity of natural language. False positives are represented by the irrelevant results that were returned by the search.<br>Clustering techniques based on **Bayesian Algorithms** can help reduce false positives. For the search term of Bank, clustering can be used to categorize the document into a financial institution, the place to sit, place to store, etc. Depending on the occurrences of words relevant to the categories, search terms or a search result can be placed in one or more of the categories. This technique is being extensively deployed in the **e-discovery** domain.
<br>
<br>

### **6. PERFORMANCE IMPROVEMENTS**

The deficiencies of free text searching have been addressed in two ways: By providing users with tools that enable them to express their search questions more precisely and by developing new search algorithms that improve retrieval precision.
<br>

**6.1.    IMPROVED QUERYING TOOLS**

* **Keywords:**  Document creators are asked t supply a list of words that describe the subject of the text, including synonyms of words that describe this subject. Keywords improve recall, particularly if the keyword list includes a search word that is not in the document text.
* **Field-restricted Search:** Some search engines enable users to limit free text searches to a particular field within a stored data record, such as Title or Author.
* **Phrase Search:** A phrase search matches only those documents that contain a specified phrase, such as Wikipedia, the free encyclopedia.
* **Concept Search:**  A search that is based on multi-word concepts, for example, compound term processing. This type of search is becoming popular in many e-discovery solutions.
* **Concordance Search:**  A concordance search produces an alphabetical list of all principal words that occur in a text with their immediate context.

**6.2. IMPROVED SEARCH ALGORITHM**
* The Pagerank algorithm developed by Google gives more permission to documents to which other web pages are linked.
<br>
<br>

### **7. SOFTWARE**

There are many Software products available whose predominant purpose is to perform full-text searching.
<br>

**7.1. FREE AND OPEN SOURCE SOFTWARE**
* **Apache Solr:** 
    * It is an open-source enterprise search platform, written in Java, from the Apache Lucene project. Its major features include full-text search hit highlighting, real-time indexing, dynamic clustering, database integration,  features, and rich document (e.g., Word, PDF) handling. 
    * In 2004, Solr was created by Yonik Seeley at CNET Networks as an in house project to add search capability for the company website. 
    * In January 2021, Solr 8.8.0 was released including many bugfixes and component updates.

* **ArangoSearch:**
    * It is a free and open-source native multi-model database system developed by ArangoDB GmbH. The database system supports three data models key/value, documents, and graphs with one database core and a unified query language AQL (ArangoDB Query Language).
    * Started in 2011, the database was originally released under the name AvocadoDB but changed to ArangoDB in 2012.

* **BaseX:**
    * It is a native and light-weight XML database management system and XQuery processor developed as a community project on GitHub. It is specialized in storing, querying, and visualizing large XML documents and collections. BaseX is platform-independent and distributed under a permissive free software license.
    * It was started by Christian Grün at the University of Konstanz in 2005. In 2007, BaseX went open source and has been BSD-licensed since then.

* **Elasticsearch:**
    * It is a search engine based on the Lucene library. It provides a distributed,multitenant-capable full-text search engine with an HTTP web interface and schema-free JSON documents. 
    * It was founded in 2012 to provide commercial services and products around Elasticsearch and related software.
<br>

**7.2.PROPRIETARY SOFTWARE**
* **Algolia:** 
    * It is a U.S. startup company offering a web search product through a SaaS (software as a service) model.
    * Algolia was founded in 2012 by Nicolas Dessaigne and Julien Lemoine.

* **Autonomy Corporation:**
    * HP Autonomy, previously Autonomy Corporation PLC, was an enterprise software company that was merged with Micro Focus in 2017. It was founded in Cambridge, the United Kingdom in 1996.
    * Autonomy was founded in Cambridge, England by Michael Lynch, David Tabizel, and Richard Gaunt in 1996 as a spin-off from Cambridge Neurodynamics, a firm specializing in computer-based fingerprint recognition.

* **Azure Search:**
    * It is a component of the Microsoft Azure Cloud Platform providing indexing and querying capabilities for data uploaded to Microsoft servers.
    * In 2008 Microsoft released the Azure platform with a cloud-based component code-named project Red Dog
<br>
<br>

### **8.CONCLUSION & FUTURE WORK**

In conclusion, the analysis of results of the implemented information retrieval system shows that the users of the system find it very effective to use. The implemented information retrieval system enables users to have access to the latest learning facilities such as articles, journals, textbooks, projects, newspapers, etc. Without going through the hard steps and routine in the conventional institution libraries.
<br>
<br>

### **9. REFERENCES**
* https://en.wikipedia.org/wiki/Full-text_search
* https://www.researchgate.net/publication/280443639_EVALUATION_OF_FULL_TEXT_SEARCH_RETRIEVAL_SYSTEM/link/57e1283708aeb801a6c0673c
* https://www.scientific.net/AMR.952.355
* https://youtu.be/GeMlUbbZhKQ
* https://youtu.be/cn2fwmvLqA0
